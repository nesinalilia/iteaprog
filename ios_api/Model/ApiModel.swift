//
//  ApiModel.swift
//  ios_api
//
//  Created by Лилия on 7/10/19.
//  Copyright © 2019 ITEA. All rights reserved.
//

import Foundation

class ApiModel {
    var id: String?
    var name: String?
    var description: String?
    var url: String?
    var category: String?
    var language: String?
    var country: String?
}
