//
//  TableViewCell.swift
//  ios_api
//
//  Created by Лилия on 7/10/19.
//  Copyright © 2019 ITEA. All rights reserved.
//

import UIKit

protocol TableViewCellDelegate {
    func toList(url: String)
}

class TableViewCell: UITableViewCell {

    @IBOutlet weak var nameNewsLabel: UILabel!
    @IBOutlet weak var websiteNewsLabel: UILabel!
    @IBOutlet weak var categoryNewsLabel: UILabel!
    @IBOutlet weak var descriptionNewsLabel: UILabel!
    
    var delegate: TableViewCellDelegate?
    var urlSrt: String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func update(news: ApiModel) {
        nameNewsLabel.text = news.name ?? "-"
        websiteNewsLabel.text = news.url ?? "-"
        categoryNewsLabel.text = news.category ?? "-"
        descriptionNewsLabel.text = news.description ?? "-"
    }
    
    @IBAction func didTapEmail(_ sender: Any) {
        if let url = urlSrt {
          delegate?.toList(url: url)
        }
    }
}
