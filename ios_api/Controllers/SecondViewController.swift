//
//  SecondViewController.swift
//  ios_api
//
//  Created by Лилия on 7/10/19.
//  Copyright © 2019 ITEA. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
   
    var nameCapitalDetail: String?
    let hostURL = "https://newsapi.org/v2/sources"
    var arrayInModel = [ApiModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        navigationController?.navigationBar.isHidden = true
        
        tableView.register(UINib(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "TableViewCell")
        tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        request()
    }
    
    func request() {
        
        if let url = URL(string: hostURL) {
            var request = URLRequest(url: url)
            
            request.httpMethod = "GET"
            request.allHTTPHeaderFields = ["X-Api-Key": "1db467d7c9a148d9bae709b09c2bb148"]
            
            let task = URLSession.shared.dataTask(with: request) {(data, response, error) in
                guard let data = data else {
                    return
                }
                do {
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                        if let status = json["status"] as? String {
                            debugPrint(status)
                            if let sources = json["sources"] as? [[String: Any]] {
                                for items in sources {
                                    let list = ApiModel()
                                    if let id = items["id"] as? String {
                                        list.id = id
                                    }
                                    if let name = items["name"] as? String {
                                        list.name = name
                                    }
                                    if let description = items["description"] as? String {
                                        list.description = description
                                    }
                                    if let url = items["url"] as? String {
                                        list.url = url
                                    }
                                    if let category = items["category"] as? String {
                                        list.category = category
                                    }
                                    self.arrayInModel.append(list)
                                }
                            }
                        }
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                        }
                    }
                }
                catch let _ as NSError {
                }
                
            }
            task.resume()
        }
    }
}

extension SecondViewController: UITableViewDelegate, UITableViewDataSource, TableViewCellDelegate {
    
    func toList(url: String) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ThirdViewController") as! ThirdViewController
        vc.urlSrt = url
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayInModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath) as! TableViewCell
        cell.update(news: arrayInModel[indexPath.row])
        cell.urlSrt = arrayInModel[indexPath.row].url ?? "-"
        cell.delegate = self
        return cell
    }
    
    
}
