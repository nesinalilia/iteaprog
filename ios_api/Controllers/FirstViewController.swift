//
//  FirstViewController.swift
//  ios_api
//
//  Created by Лилия on 7/10/19.
//  Copyright © 2019 ITEA. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {
    @IBOutlet weak var startButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        startButton.layer.borderColor = UIColor.yellow.cgColor
        startButton.layer.borderWidth = 1
        startButton.clipsToBounds = true
        startButton.layer.cornerRadius = 25
        
        navigationController?.navigationBar.isHidden = true
    }
    
    @IBAction func didTapStartNews(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "SecondViewController") as! SecondViewController
        navigationController?.pushViewController(vc, animated: true)
    }
    
}
