//
//  ThirdViewController.swift
//  ios_api
//
//  Created by Лилия on 7/11/19.
//  Copyright © 2019 ITEA. All rights reserved.
//

import UIKit
import WebKit

class ThirdViewController: UIViewController {
    
    @IBOutlet weak var webView: WKWebView!
    
    var urlSrt: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = false
        if let url1 = URL(string: urlSrt) {
            let urlRequest = URLRequest(url: url1)
            webView.load(urlRequest)
            
        }
    }
}
