//
//  BranchCompanyModel.swift
//  DzLili4
//
//  Created by Лилия on 6/28/19.
//  Copyright © 2019 ITEA. All rights reserved.
//

import Foundation

class BranchCompanyModel {
    
    var image: String
    var name: String
    var location: String
    var phone: String
    var latitude: Float
    var longitude: Float
    
    init(image: String, name: String, location: String, phone: String, latitude: Float, longitude: Float) {
        self.image = image
        self.name = name
        self.location = location
        self.phone = phone
        self.latitude = latitude
        self.longitude = longitude
    }
}
