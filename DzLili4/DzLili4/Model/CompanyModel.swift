//
//  CompanyModel.swift
//  DzLili4
//
//  Created by Лилия on 6/28/19.
//  Copyright © 2019 ITEA. All rights reserved.
//

import Foundation

class CompanyModel {
    
    var image: String
    var name: String
    var location: String
    var phone: String
    var email: String
    var site: String
    var specialists: String
    var cityOfBranch: String
    var valuation: String
    var imageDetail: String
    var latitude: Float
    var longitude: Float
    var branch: [BranchCompanyModel]
    
    
    init(image: String, name: String, location: String, phone: String, email: String, site: String, specialists: String, cityOfBranch: String, valuation: String, imageDetail: String, latitude: Float, longitude: Float, branch: [BranchCompanyModel]) {
        self.image = image
        self.name = name
        self.location = location
        self.phone = phone
        self.email = email
        self.site = site
        self.specialists = specialists
        self.cityOfBranch = cityOfBranch
        self.valuation = valuation
        self.imageDetail = imageDetail
        self.latitude = latitude
        self.longitude = longitude
        self.branch = branch
    }
}
