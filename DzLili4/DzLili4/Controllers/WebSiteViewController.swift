//
//  WebSiteViewController.swift
//  DzLili4
//
//  Created by Лилия on 6/29/19.
//  Copyright © 2019 ITEA. All rights reserved.
//

import UIKit
import WebKit

class WebSiteViewController: UIViewController {

    @IBOutlet weak var webViewCompany: WKWebView!
    
    var urlSrt: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webCourses(courseUrl: urlSrt)
//        if let url1 = URL(string: urlSrt) {
//            let urlRequest = URLRequest(url: url1)
//            webViewCompany.load(urlRequest)
//        }

    }

    func webCourses(courseUrl: String)  {
        guard let myUrl = URL(string: courseUrl), courseUrl != "" else {
            return
        }
        
        let urlRequest = URLRequest(url: myUrl)
        webViewCompany.load(urlRequest)
        return 
    }
    @IBAction func didTapBackViewController(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
