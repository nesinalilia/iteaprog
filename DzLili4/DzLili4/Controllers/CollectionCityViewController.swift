//
//  CollectionCityViewController.swift
//  DzLili4
//
//  Created by Лилия on 6/30/19.
//  Copyright © 2019 ITEA. All rights reserved.
//

import UIKit

class CollectionCityViewController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    
    var arrayCityOfBranch: [BranchCompanyModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let layout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.sectionInset = UIEdgeInsets(top: 0, left: 7, bottom: 0, right: 7)
        layout.minimumInteritemSpacing = 5
        layout.itemSize = CGSize(width: (self.collectionView.frame.size.width - 20)/2, height: (self.collectionView.frame.size.height)/4)
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CollectionViewCell")

    }

    @IBAction func didTapBackViewController(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}


extension CollectionCityViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayCityOfBranch.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as! CollectionViewCell
        item.update(city: arrayCityOfBranch[indexPath.row])
        item.imageBranchCity.layer.shadowOffset = CGSize(width: 5, height: 5)
        item.imageBranchCity.layer.shadowOpacity = 0.7
        item.imageBranchCity.layer.shadowRadius = 1
        item.imageBranchCity.layer.shadowColor = UIColor(red: 44.0/255.0, green: 62.0/255.0, blue: 80.0/255.0, alpha: 1.0).cgColor
        return item
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "MapBranchViewController") as! MapBranchViewController
//        vc.nameCourse = arrayCourses[indexPath.row].nameCourses
//        vc.nameTypes = arrayCourses[indexPath.row].nameTypeCourses
        vc.cityOfBranchMap = arrayCityOfBranch[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
