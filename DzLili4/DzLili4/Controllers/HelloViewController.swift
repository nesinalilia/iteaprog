//
//  HelloViewController.swift
//  DzLili4
//
//  Created by Лилия on 6/28/19.
//  Copyright © 2019 ITEA. All rights reserved.
//

import UIKit

class HelloViewController: UIViewController {
    @IBOutlet weak var helloView: UIView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        helloView.clipsToBounds = true
        helloView.layer.cornerRadius = 20
//        helloView.layer.borderColor = #colorLiteral(red: 0.2517604232, green: 0.3400406837, blue: 0.6864091158, alpha: 1)
//        helloView.layer.borderWidth = 0.5
        
        navigationController?.navigationBar.isHidden = true
        
    }
    
    @IBAction func didTapGetStarted(_ sender: Any) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "MapsCompanyViewController") as! MapsCompanyViewController
        navigationController?.pushViewController(vc, animated: true)
    }
}
