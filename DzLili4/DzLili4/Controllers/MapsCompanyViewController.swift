//
//  MapsCompanyViewController.swift
//  DzLili4
//
//  Created by Лилия on 6/28/19.
//  Copyright © 2019 ITEA. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class MapsCompanyViewController: UIViewController {
    
    @IBOutlet weak var mapViewCompany: GMSMapView!
    @IBOutlet weak var shadowView: UIView!
    
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var imageCompany: UIImageView!
    @IBOutlet weak var nameCompanyLabel: UILabel!
    @IBOutlet weak var locationCompanyLabel: UILabel!
    @IBOutlet weak var phoneCompanyLabel: UILabel!
    @IBOutlet weak var emailCompanyLabel: UILabel!
    
    var locationManager = CLLocationManager()
    var latitudeUkraine: Float = 50.4505091
    var longitudeUkraine: Float = 30.5255127
    
    var arrayCompany = CompanyManager().companies
    var companyTemp: CompanyModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setCurrentLocation()
        navigationController?.navigationBar.isHidden = true
        
        mapViewCompany.clipsToBounds = true
        mapViewCompany.layer.cornerRadius = 30
        mapViewCompany.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]

    }
    
    func Alert(info: String) {
        let alert = UIAlertController(title: info, message: "Какую информацию показать?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Краткая инф.", style: .default, handler: showLessInfo))
        
        alert.addAction(UIAlertAction(title: "Подробнее->", style: .default, handler: showMoreInfo))
        self.present(alert,animated: true)
    }
    
    func showMoreInfo(alert: UIAlertAction!) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "DetailCompanyViewController") as! DetailCompanyViewController
        navigationController?.pushViewController(vc, animated: true)
        vc.companyModeldetail = companyTemp
    }
    
    func showLessInfo(alert: UIAlertAction!) {
        shadowView.isHidden = false
        infoView.clipsToBounds = true
        infoView.layer.cornerRadius = 15
        
    }
    
    @IBAction func didTapCloseShadowView(_ sender: Any) {
        shadowView.isHidden = true
    }
    
}

extension MapsCompanyViewController: CLLocationManagerDelegate, GMSMapViewDelegate {
    
    func setCurrentLocation() {
        locationManager.delegate = self
        mapViewCompany.delegate = self
        mapViewCompany.isMyLocationEnabled = true
        locationManager.startUpdatingLocation()
        
    }
    
    func setMarker(arrayCompanyModel: CompanyModel, lat: Double, lon: Double ) {
        
        var userInfo: [String: Any] = [:]
        
        userInfo["name"] = arrayCompanyModel.name
        userInfo["image"] = arrayCompanyModel.image
        userInfo["phone"] = arrayCompanyModel.phone
        userInfo["email"] = arrayCompanyModel.email
        userInfo["adress"] = arrayCompanyModel.location
        userInfo["site"] = arrayCompanyModel.site
        
        
        
        let marker = GMSMarker(position: CLLocationCoordinate2D(latitude: CLLocationDegrees(arrayCompanyModel.latitude), longitude: CLLocationDegrees(arrayCompanyModel.longitude)))
        
        marker.title = arrayCompanyModel.name
        
        let markerImage = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        markerImage.image = UIImage(named: "pin3")
        marker.iconView = markerImage
        marker.userData = userInfo
        marker.map = mapViewCompany
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(latitudeUkraine), longitude: CLLocationDegrees(longitudeUkraine), zoom: 6.0)
        self.mapViewCompany.animate(to: camera)
        
        for company in arrayCompany {
            setMarker(arrayCompanyModel: company, lat: Double(company.latitude), lon: Double(company.longitude))
        }
        
       // for museum in museumArray {
            
//            let marker = GMSMarker(position: CLLocationCoordinate2D(latitude: CLLocationDegrees(latitudeUkraine), longitude: CLLocationDegrees(longitudeUkraine)))
//            marker.title = museum.nameMuseum
//            marker.snippet = museum.descriptionMuseum
//            nameMuseum = museum.nameMuseum
//            marker.icon = GMSMarker.markerImage(with: .red)
//            marker.map = mapViewCompany
       // }
        self.locationManager.stopUpdatingLocation()
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        for company in arrayCompany {
            if marker.title == company.name {
                let nameTemp = marker.title ?? "No name"
                Alert(info: "IT Компания \(nameTemp)")
                companyTemp = company
                
                imageCompany.image = UIImage(named: company.image)
                nameCompanyLabel.text = company.name
                locationCompanyLabel.text = company.location
                phoneCompanyLabel.text = company.phone
                emailCompanyLabel.text = company.email
            }
        
        }
        return true
    }
    
}
