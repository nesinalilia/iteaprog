//
//  MapBranchViewController.swift
//  DzLili4
//
//  Created by Лилия on 6/30/19.
//  Copyright © 2019 ITEA. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class MapBranchViewController: UIViewController {

    @IBOutlet weak var branchMapView: GMSMapView!
    @IBOutlet weak var cityOfBranchLabel: UILabel!
    
    var cityOfBranchMap: BranchCompanyModel?
    var locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        branchMapView.clipsToBounds = true
        branchMapView.layer.cornerRadius = 30
        branchMapView.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        
        cityOfBranchLabel.text = cityOfBranchMap?.name
    }
    
    @IBAction func didTapBackViewController(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}

extension MapBranchViewController: CLLocationManagerDelegate, GMSMapViewDelegate {
    
    func setCurrentLocation() {
        locationManager.delegate = self
        branchMapView.delegate = self
        branchMapView.isMyLocationEnabled = true
        locationManager.startUpdatingLocation()
        
    }
    
    func setMarker(arrayBranchCompanyModel: BranchCompanyModel, lat: Double, lon: Double ) {
        
        var userInfo: [String: Any] = [:]

        userInfo["name"] = arrayBranchCompanyModel.name
        userInfo["image"] = arrayBranchCompanyModel.image
        userInfo["phone"] = arrayBranchCompanyModel.phone
        userInfo["adress"] = arrayBranchCompanyModel.location
        
        
        
        let marker = GMSMarker(position: CLLocationCoordinate2D(latitude: CLLocationDegrees(cityOfBranchMap?.latitude ?? 0.0), longitude: CLLocationDegrees(cityOfBranchMap?.longitude ?? 0.0)))
        
        let markerImage = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        markerImage.image = UIImage(named: "pin3")
        marker.iconView = markerImage
        marker.userData = userInfo
        marker.map = branchMapView
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(cityOfBranchMap?.latitude ?? 0.0), longitude: CLLocationDegrees(cityOfBranchMap?.longitude ?? 0.0), zoom: 12.0)
        self.branchMapView.animate(to: camera)
        
        self.locationManager.stopUpdatingLocation()
    }
    
    
}
