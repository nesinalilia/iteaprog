//
//  DetailCompanyViewController.swift
//  DzLili4
//
//  Created by Лилия on 6/28/19.
//  Copyright © 2019 ITEA. All rights reserved.
//

import UIKit

class DetailCompanyViewController: UIViewController {

    @IBOutlet weak var companyView: UIView!
    @IBOutlet weak var imageCompany: UIImageView!
    @IBOutlet weak var nameCompanyLabel: UILabel!
    @IBOutlet weak var numberOfSpecialistsLabel: UILabel!
    @IBOutlet weak var cityOfBranchLabel: UILabel!
    @IBOutlet weak var webSiteCompanyLabel: UILabel!
    @IBOutlet weak var phonaCompanyLabel: UILabel!
    @IBOutlet weak var emailCompanyLabel: UILabel!
    @IBOutlet weak var valuationCompanyLabel: UILabel!
    @IBOutlet weak var imageCompanyDetail: UIImageView!
    

    var companyModeldetail: CompanyModel?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        companyView.clipsToBounds = true
        companyView.layer.cornerRadius = 30
        companyView.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        
        imageCompanyDetail.layer.borderColor = #colorLiteral(red: 0.3106389344, green: 0.3687070608, blue: 0.5537250042, alpha: 1)
        imageCompanyDetail.layer.borderWidth = 0.5
        imageCompanyDetail.clipsToBounds = true
        imageCompanyDetail.layer.cornerRadius = 15
        
        imageCompany.image = UIImage(named: companyModeldetail?.image ?? "-")
        nameCompanyLabel.text = companyModeldetail?.name ?? "-"
        numberOfSpecialistsLabel.text = companyModeldetail?.specialists ?? "-"
        cityOfBranchLabel.text = companyModeldetail?.cityOfBranch ?? "-"
        webSiteCompanyLabel.text = companyModeldetail?.site ?? "-"
        phonaCompanyLabel.text = companyModeldetail?.phone ?? "-"
        emailCompanyLabel.text = companyModeldetail?.email ?? "-"
        valuationCompanyLabel.text = companyModeldetail?.valuation ?? "-"
        imageCompanyDetail.image = UIImage(named: companyModeldetail?.imageDetail ?? "-") 
        

    }
    
    @IBAction func didTapWebSiteCompany(_ sender: Any) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "WebSiteViewController") as! WebSiteViewController
        vc.urlSrt = companyModeldetail?.site ?? "-"
        navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func didTapCityOfBranch(_ sender: Any) {
       
        let vc = storyboard?.instantiateViewController(withIdentifier: "CollectionCityViewController") as! CollectionCityViewController
        vc.arrayCityOfBranch = companyModeldetail?.branch ?? []
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func didTapCloseView(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
