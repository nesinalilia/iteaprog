//
//  CollectionViewCell.swift
//  DzLili4
//
//  Created by Лилия on 6/30/19.
//  Copyright © 2019 ITEA. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageBranchCity: UIImageView!
    @IBOutlet weak var nameBranchCityLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imageBranchCity.clipsToBounds = true
        imageBranchCity.layer.cornerRadius = 15
        
    }
    
    func update(city: BranchCompanyModel) {
        imageBranchCity.image = UIImage(named: city.image)
        nameBranchCityLabel.text = city.name
    }

}
