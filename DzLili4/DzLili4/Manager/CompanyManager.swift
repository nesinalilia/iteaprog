//
//  CompanyManager.swift
//  DzLili4
//
//  Created by Лилия on 6/28/19.
//  Copyright © 2019 ITEA. All rights reserved.
//

import Foundation


class CompanyManager {
    
    var companies: [CompanyModel] = []
    
    init() {
        
        companies.append(CompanyModel(image: "epam", name: "EPAM", location: "Киев, ул. Жилянська 75", phone: "+380 (44) 390 54 57", email: "support@dou.ua", site: "https://www.epam.com", specialists: "от 1500 специалистов", cityOfBranch: "Киев, Харьков, Львов, Днепр, Винница", valuation: "85/100", imageDetail: "epamD", latitude: 50.4389983, longitude: 30.4988322, branch: [BranchCompanyModel(image: "kharkov", name: "Харьков", location: "Харьков, ул. Сумська, 10", phone: "+38(044) 503 43 33", latitude: 49.9945194, longitude: 36.2328005), BranchCompanyModel(image: "lvov", name: "Львов", location: "Львов, ул. Олени Степанівни, 45", phone: "+380 (44) 503 43 33", latitude: 49.8427904, longitude: 3.9995774), BranchCompanyModel(image: "dnepr", name: "ДНЕПР", location: "Днепр, ул. Мономаха, 17 А", phone: "+380 (56) 790 56 51", latitude: 48.4677113, longitude: 35.0452836), BranchCompanyModel(image: "vinitza", name: "Винница", location: "Винница, ул. Миколи Оводова, 51", phone: "+380 (43) 255 12 94", latitude: 49.233706, longitude: 28.4708362)]))
        
        companies.append(CompanyModel(image: "softserve", name: "SoftServe", location: "Киев, ул. Лейпцизька 15", phone: "+380 (32) 290 14 42", email: "hr@softserveinc.com", site: "https://www.softserveinc.com", specialists: "от 1500 специалистов", cityOfBranch: "Киев, Харьков, Львов, Днепр, Ровно", valuation: "88/100", imageDetail: "softD", latitude: 50.4305544, longitude: 30.5496359, branch: [BranchCompanyModel(image: "kharkov", name: "Харьков", location: "Харьков, ул. Сумська 10", phone: "+380 (57) 727 53 60", latitude: 49.994481, longitude: 36.2328189), BranchCompanyModel(image: "lvov", name: "Львов", location: "Львов, ул. Героїв УПА, 72", phone: "+380 (32) 240 99 99", latitude: 49.8313239, longitude: 23.9969465), BranchCompanyModel(image: "dnepr", name: "Днепр", location: "Днепр, пр. Карла Маркса, 22", phone: "+380 (56) 790 22 72", latitude: 48.4579374, longitude: 35.059797), BranchCompanyModel(image: "rovno", name: "Ровно", location: "Ровно, ул. Словацького 4/6", phone: "+380 (36) 263 46 63", latitude: 50.6220078, longitude: 26.2499088)]))
        
        companies.append(CompanyModel(image: "nix", name: "NIX", location: "Харьков, ул. Каразина, 2", phone: "+380 (57) 784 06 00", email: "info@nixsolutions.com", site: "https://www.nixsolutions.com", specialists: "от 1500 специалистов", cityOfBranch: "Харьков", valuation: "80/100", imageDetail: "nixD", latitude: 50.0083078, longitude: 36.2392745, branch: [BranchCompanyModel(image: "kharkov", name: "Харьков", location: "Харьков, ул. Каразина, 2", phone: "+380 (57) 784 06 00", latitude: 50.0083078, longitude: 36.2392745), BranchCompanyModel(image: "kharkov", name: "Харьков", location: "Харьков, проспект Правды 10", phone: "+380 (57) 766 07 13", latitude: 50.0062022, longitude: 36.2342174)]))
        
        companies.append(CompanyModel(image: "evo", name: "EVO", location: "Киев, Харьковское Шоссе 201/203", phone: "+380 (44) 593 72 40", email: "cv@evo.company", site: "https://evo.company", specialists: "800...1500 специалистов", cityOfBranch: "Киев", valuation: "93/100", imageDetail: "evoD", latitude: 50.4048144, longitude: 30.6815087, branch: [BranchCompanyModel(image: "kiev", name: "Киев", location: "Киев, бул. Вацлава Гавела, 6", phone: "+380 (44) 593 77 94", latitude: 50.447638, longitude: 30.4222701), BranchCompanyModel(image: "kiev", name: "Киев", location: "Киев, ул. Брюллова, 7", phone: "+380 (44) 360 18 29", latitude: 50.440282, longitude: 30.481904)]))
        
        companies.append(CompanyModel(image: "ciklum", name: "Ciklum", location: "Львов, ул. Шевченко 111a", phone: "+380 (44) 545 77 45 ", email: "hr@ciklum.com", site: "https://www.ciklum.com", specialists: "от 1500 специалистов", cityOfBranch: "Харьков, Львов, Днепр, Винница", valuation: "80/100", imageDetail: "ciklumD", latitude: 49.8505041, longitude: 23.9912731, branch: [BranchCompanyModel(image: "kharkov", name: "Харьков", location: "Харьков, ул. Отакара Яроша, 18Д", phone: "+380 (44) 498 16 98 ", latitude: 50.0271263, longitude: 36.2206012),BranchCompanyModel(image: "vinitza", name: "Винница", location: "ул. Соборная, 24, ТЦ «Магицентр»", phone: "+380 (43) 250 82 85", latitude: 49.2328817, longitude: 28.4718973), BranchCompanyModel(image: "odessa", name: "Одесса", location: "Одесса, ул. Леха Качинского, 7", phone: "+380 (56) 375 71 98", latitude: 46.4824753, longitude: 30.7450936)]))
        
        companies.append(CompanyModel(image: "lohika", name: "Lohika", location: "Одесса, ул. Бунина, 1", phone: "+380 (32) 232 52 42 ", email: "job_odessa@lohika.com", site: "https://www.lohika.com.ua", specialists: "800...1500 специалистов", cityOfBranch: "Киев, Одесса", valuation: "92/100", imageDetail: "lohikaD", latitude: 46.4801971, longitude: 30.7478459, branch: [BranchCompanyModel(image: "kiev", name: "Киев", location: "Киев, ул. Жилянская 35", phone: "+380 (44) 593 80 80", latitude: 50.4338903, longitude: 30.5090926)]))
        
        companies.append(CompanyModel(image: "ELEKS", name: "ELEKS", location: "Тернополь, ул. Оболоня, 49", phone: "+380 (35) 240 00 96", email: "eleksinfo@eleks.com", site: "https://www.eleks.com", specialists: "800...1500 специалистов", cityOfBranch: "Киев, Тернополь, Ивано-Франковск", valuation: "88/100", imageDetail: "eleksD", latitude: 49.5422781, longitude: 25.5928802, branch: [BranchCompanyModel(image: "kiev", name: "Киев", location: "Киев, ул. Петра Сагайдачного, 11", phone: "+380 (32) 297 12 51", latitude: 50.4584693, longitude: 30.5251528), BranchCompanyModel(image: "ivano_f", name: "Ивано-франковск", location: "Ивано-Франковск, Північний бульвар, 2А", phone: "+380 (35) 240 00 96", latitude: 48.9296479, longitude: 24.7073951)]))
}
}
