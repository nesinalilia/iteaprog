import UIKit

class Student {
    var age: Int = Int()
    var name: String = String()
}

let student1 = Student()
student1.age = 20
student1.name = "Ala"

let student2 = Student()
student2.age = 21
student2.name = "Max"

let student3 = Student()
student3.age = 16
student3.name = "Alexsandr"

let student4 = Student()
student4.age = 18
student4.name = "Vera"

let student5 = Student()
student5.age = 25
student5.name = "Liliia"

var arrayStudent: [Student] = [student1, student2, student3, student4, student5]

//СОРТИРОВКА СТУДЕНТОВ ПО ВОЗРАСТУ
let sortByAgeClouser: ([Student]) -> [Student] = { (student) in
    let sortArray = student.sorted(by: {$0.age > $1.age})
    return sortArray
}

print("--------SortedByAge-------")
for student in sortByAgeClouser(arrayStudent) {
    print("\(student.name) - \(student.age)")
}

//СОРТИРОВКА СТУДЕНТОВ ПО ИМЕНИ
let sortByNameClouser: ([Student]) -> [Student] = { (student) in
    let sortArray = student.sorted(by: {$0.name < $1.name})
    return sortArray
}

print("--------SortedByName-------")
for student in sortByNameClouser(arrayStudent) {
    print(student.name)
}

//ФИЛЬТРАЦИЯ ПО ВОЗВРАСТУ
let filterByAgeClouserVoid: ([Student]) -> Void = { (student) in
    arrayStudent = student.filter({$0.age % 2 == 0})
}

filterByAgeClouserVoid(arrayStudent)
print("--------FilterByAge-------")
for student in arrayStudent {
    print("\(student.name) - \(student.age)")
}

//ФИЛЬТРАЦИЯ ПО ИМЕНИ
let filterByNameClouser: ([Student]) -> [Student] = { (student) in
    let filterArray = student.filter({$0.name.count < 4})
    return filterArray
}

print("--------FilterByName-------")
for student in filterByNameClouser(arrayStudent) {
    print(student.name)
}

var arrayStudent2: [Student] = [student1, student2, student3, student4, student5]

//ФИЛЬТРАЦИЯ ПО ИМЕНИ А ПОТОМ СОРТИРОВКА ПО ВОЗРАСТУ

var completionHandler: ([Student]) -> Void = { (student) in
    arrayStudent2 = student.filter({$0.name.first == "A" }).sorted(by: {$0.age < $1.age})
}

completionHandler(arrayStudent2)
print("--------filterDoubleClosures-------")
for student in arrayStudent2 {
    print(student.name)
}

