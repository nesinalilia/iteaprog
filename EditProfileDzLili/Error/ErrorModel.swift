//
//  ErrorModel.swift
//  EditProfileDzLili
//
//  Created by Лилия on 7/8/19.
//  Copyright © 2019 ITEA. All rights reserved.
//

import Foundation

enum ErrorModel: String {
    case errorName = "Введено некоректное ИМЯ пользователя"
    case errorSurname = "Введена некоректная ФАМИЛИЯ пользователя"
    case errorAge = "Введено некоректное значение ВОЗРАСТА пользователя"
    case errorEmail = "Введен некоректный адрес EMAIL пользователя"
    case errorPhone = "Введен некоректный НОМЕР ТЕЛЕФОНА пользователя"
    case errorCity = "Введен некоректный ГОРОД пользователя"
}

//var a : Dictionary < String , String > = [:]
//var a : [String : String ] = [:]
