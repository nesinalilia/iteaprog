//
//  PersonManager.swift
//  EditProfileDzLili
//
//  Created by Лилия on 7/7/19.
//  Copyright © 2019 ITEA. All rights reserved.
//

import Foundation

class PersonManager {
    var person: [PersonModel] = []
    
    init() {
        person.append(PersonModel(name: "Лилия", surname: "Несина", age: "21" , email: "lilianesina@gmail.com", phone: "+380(99)7792763", city: "Киев"))
    }
}
