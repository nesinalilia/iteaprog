//
//  PersonModel.swift
//  EditProfileDzLili
//
//  Created by Лилия on 7/7/19.
//  Copyright © 2019 ITEA. All rights reserved.
//

import Foundation

class PersonModel {
    var name: String
    var surname: String
    var age: String
    var email: String
    var phone: String
    var city: String
    
    init(name: String, surname: String, age: String, email: String, phone: String, city: String) {
        self.name = name
        self.surname = surname
        self.age = age
        self.email = email
        self.phone = phone
        self.city = city
    }
    
}
