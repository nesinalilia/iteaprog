//
//  SaveProfileViewController.swift
//  EditProfileDzLili
//
//  Created by Лилия on 7/5/19.
//  Copyright © 2019 ITEA. All rights reserved.
//

import UIKit

class SaveProfileViewController: UIViewController {

    @IBOutlet weak var nameSaveLabel: UILabel!
    @IBOutlet weak var surnameSaveLabel: UILabel!
    @IBOutlet weak var ageSaveLabel: UILabel!
    @IBOutlet weak var emailSaveLabel: UILabel!
    @IBOutlet weak var phoneSaveLabel: UILabel!
    @IBOutlet weak var citySaveLabel: UILabel!
    
    var name: String = ""
    var surname: String = ""
    var age: String = ""
    var email: String = ""
    var phone: String = ""
    var city: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameSaveLabel.text = name
        surnameSaveLabel.text = surname
        ageSaveLabel.text = age
        emailSaveLabel.text = email
        phoneSaveLabel.text = phone
        citySaveLabel.text = city
    }
    
    @IBAction func didTapEditProfile(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
