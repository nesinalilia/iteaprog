//
//  EditProfileViewController.swift
//  EditProfileDzLili
//
//  Created by Лилия on 7/5/19.
//  Copyright © 2019 ITEA. All rights reserved.
//

import UIKit

class EditProfileViewController: UIViewController {
    @IBOutlet weak var profileView: UIView!
    
    @IBOutlet weak var namePersonTextField: UITextField!
    @IBOutlet weak var errorNameLabel: UILabel!
    @IBOutlet weak var errorNameHeightConstrain: NSLayoutConstraint!
    
    @IBOutlet weak var surnamePersonTextField: UITextField!
    @IBOutlet weak var errorSurnameLabel: UILabel!
    @IBOutlet weak var errorSurnameHeightConstrain: NSLayoutConstraint!
    
    @IBOutlet weak var agePersonTextField: UITextField!
    @IBOutlet weak var errorAgeLabel: UILabel!
    @IBOutlet weak var errorAgeHeightConstrain: NSLayoutConstraint!
    
    @IBOutlet weak var emailPersonTextField: UITextField!
    @IBOutlet weak var errorEmailLabel: UILabel!
    @IBOutlet weak var errorEmailHeightConstrain: NSLayoutConstraint!
    
    @IBOutlet weak var phonePersonTextField: UITextField!
    @IBOutlet weak var errorPhoneLabel: UILabel!
    @IBOutlet weak var errorPhoneHeightConstrain: NSLayoutConstraint!
    
    @IBOutlet weak var cityPersonTextField: UITextField!
    @IBOutlet weak var errorCityLabel: UILabel!
    @IBOutlet weak var errorCityHeightConstrain: NSLayoutConstraint!
    
    var person = PersonManager().person
    
    var arrayDataPerson: PersonModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        namePersonTextField.delegate = self
        surnamePersonTextField.delegate = self
        agePersonTextField.delegate = self
        emailPersonTextField.delegate = self
        phonePersonTextField.delegate = self
        cityPersonTextField.delegate = self
        
        hideKeyboardWhenTappedAround()
        
        designTextField(textField: namePersonTextField, color: .lightGray)
        designTextField(textField: surnamePersonTextField, color: .lightGray)
        designTextField(textField: agePersonTextField, color: .lightGray)
        designTextField(textField: emailPersonTextField, color: .lightGray)
        designTextField(textField: phonePersonTextField, color: .lightGray)
        designTextField(textField: cityPersonTextField, color: .lightGray)
        
        navigationController?.navigationBar.isHidden = true
        
        updatePersonData(person: person[0])
        
    }
    
    
    func designTextField(textField: UITextField, color: UIColor) {
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(origin: CGPoint(x: 0, y: textField.frame.height - 1), size: CGSize(width: textField.frame.width, height:  1))
        bottomLine.backgroundColor = color.cgColor
        textField.borderStyle = UITextField.BorderStyle.none
        textField.layer.addSublayer(bottomLine)
    }
    
    
    func updatePersonData(person: PersonModel?) {
        namePersonTextField.text = person?.name
        surnamePersonTextField.text = person?.surname
        agePersonTextField.text =  person?.age
        emailPersonTextField.text = person?.email
        phonePersonTextField.text = person?.phone
        cityPersonTextField.text = person?.city
    }
    
    
    @IBAction func didTapSaveData(_ sender: Any) {
        
        if namePersonTextField.text == "" || namePersonTextField.text!.count < 2 || namePersonTextField.text!.count > 60 {
            showError(errorLaber: errorNameLabel, errorHeightConstrain: errorNameHeightConstrain)
            designTextField(textField: namePersonTextField, color: .red)
            errorNameLabel.text = ErrorModel.errorName.rawValue
        }
        
        if surnamePersonTextField.text == "" || surnamePersonTextField.text!.count < 2 || surnamePersonTextField.text!.count > 60 {
            showError(errorLaber: errorSurnameLabel, errorHeightConstrain: errorSurnameHeightConstrain)
            designTextField(textField: surnamePersonTextField, color: .red)
            errorSurnameLabel.text = ErrorModel.errorSurname.rawValue
        }
        
        let ageInt = Int(agePersonTextField.text ?? "0")
        if agePersonTextField.text == "" ||  ageInt! > 100 || agePersonTextField.text?.first == "0" {
            showError(errorLaber: errorAgeLabel, errorHeightConstrain: errorAgeHeightConstrain)
            designTextField(textField: agePersonTextField, color: .red)
            errorAgeLabel.text = ErrorModel.errorAge.rawValue
        }
        
        if emailPersonTextField.text == "" || !(emailPersonTextField.text?.contains("@"))! {
            showError(errorLaber: errorEmailLabel, errorHeightConstrain: errorEmailHeightConstrain)
            designTextField(textField: emailPersonTextField, color: .red)
            errorEmailLabel.text = ErrorModel.errorEmail.rawValue
        }
        
        if phonePersonTextField.text == "" || !((phonePersonTextField.text?.contains("+"))!) || phonePersonTextField.text!.count < 9 || phonePersonTextField.text!.count > 15 {
            showError(errorLaber: errorPhoneLabel, errorHeightConstrain: errorPhoneHeightConstrain)
            designTextField(textField: phonePersonTextField, color: .red)
            errorPhoneLabel.text = ErrorModel.errorPhone.rawValue
        }
        
        if cityPersonTextField.text != "" {
            if cityPersonTextField.text!.count < 3 {
                showError(errorLaber: errorCityLabel, errorHeightConstrain: errorCityHeightConstrain)
                designTextField(textField: cityPersonTextField, color: .red)
                errorCityLabel.text = ErrorModel.errorCity.rawValue
            }
        }
        
        if namePersonTextField.text != "" && namePersonTextField.text!.count > 2 && namePersonTextField.text!.count < 60 && surnamePersonTextField.text != "" && surnamePersonTextField.text!.count > 2 && surnamePersonTextField.text!.count < 60 && agePersonTextField.text != "" &&  ageInt! < 100 && agePersonTextField.text?.first != "0" && emailPersonTextField.text != "" && (emailPersonTextField.text?.contains("@"))! && phonePersonTextField.text != "" && (phonePersonTextField.text?.contains("+"))! && phonePersonTextField.text!.count > 9 && phonePersonTextField.text!.count < 16 && (cityPersonTextField.text == "" || cityPersonTextField.text!.count > 3) {
            
            
            let vc = storyboard?.instantiateViewController(withIdentifier: "SaveProfileViewController") as! SaveProfileViewController
            vc.name = namePersonTextField.text ?? "--"
            vc.surname = surnamePersonTextField.text ?? "--"
            vc.age = agePersonTextField.text ?? "--"
            vc.email = emailPersonTextField.text ?? "--"
            vc.phone = phonePersonTextField.text ?? "--"
            vc.city = cityPersonTextField.text ?? "--"
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func showError(errorLaber: UILabel, errorHeightConstrain: NSLayoutConstraint) {
        errorHeightConstrain.priority = UILayoutPriority(rawValue: 600)
        errorLaber.textColor = UIColor.red
        self.view.layoutIfNeeded()
    }
}

extension EditProfileViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(origin: CGPoint(x: 0, y: textField.frame.height - 1), size: CGSize(width: textField.frame.width, height:  1))
        bottomLine.backgroundColor = UIColor.lightGray.cgColor
        textField.borderStyle = UITextField.BorderStyle.none
        textField.layer.addSublayer(bottomLine)
        errorNameHeightConstrain.priority = UILayoutPriority(rawValue: 800)
        errorSurnameHeightConstrain.priority = UILayoutPriority(rawValue: 800)
        errorAgeHeightConstrain.priority = UILayoutPriority(rawValue: 800)
        errorEmailHeightConstrain.priority = UILayoutPriority(rawValue: 800)
        errorPhoneHeightConstrain.priority = UILayoutPriority(rawValue: 800)
        errorCityHeightConstrain.priority = UILayoutPriority(rawValue: 800)
    }
}
